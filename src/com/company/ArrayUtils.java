package com.company;

/**
 * @author kirill.akhramenok
 */
public class ArrayUtils {
    public static void printArray(int[] array) {
        for (int i = 0; i < array.length; i++) {
            System.out.print(array[i] + " ");
        }
    }

    public static int[] insertAtStart(int[] array, int... values) {
        int[] newArray = new int[array.length + values.length];
        for (int i = 0; i < values.length; i++) {
            newArray[i] = values[i];
        }
        for (int i = values.length, j = 0; i < newArray.length; i++, j++) {
            newArray[i] = array[j];
        }
        return newArray;
    }

    public static int[] insertAtTheEnd(int[] array, int... values) {
        int[] newArray = new int[array.length + values.length];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[i];
        }
        for (int i = array.length, j = 0; i < newArray.length; i++, j++) {
            newArray[i] = values[j];
        }
        return newArray;
    }

    public static int[] insertByIndex(int[] array, int index, int... values) {
        int[] newArray = new int[array.length + values.length];
        for (int i = 0; i < index; i++) {
            newArray[i] = array[i];
        }
        for (int i = index, j = 0; i < values.length + index; i++, j++) {
            newArray[i] = values[j];
        }
        for (int i = values.length + index, j = index; i < newArray.length; i++, j++) {
            newArray[i] = array[j];
        }
        return newArray;
    }

    public static int[] deleteElement(int[] array, int value) {
        int counter = 0;
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value) {
                counter++;
            } else {
                array[i - counter] = array[i];
            }
        }
        int[] newArray = new int[array.length - counter];
//        for (int i = 0, j = 0; i < array.length; i++) {
//            if (array[i] != value) {
//                newArray[j] = array[i];
//                j++;
//            }
//        }
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = array[i];
        }
        return newArray;
    }

    public static int[] reverse(int[] array) {
        int[] newArray = new int[array.length];
        for (int i = 0; i < array.length; i++) {
            newArray[i] = array[array.length - 1 - i];
//            newArray[array.length - 1 - i] = array[i];
        }
        return newArray;
    }

//    public static int[] sort(int[] array) {
//
//    }
}
