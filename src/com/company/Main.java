package com.company;

import java.util.Random;

public class Main {
    public static void main(String[] args) {
        int[] array = new int[10];
        Random random = new Random();
        for (int i = 0; i < array.length; i++) {
            array[i] = random.nextInt(10);
        }
        ArrayUtils.printArray(array);
        System.out.println("----------------------");
//        ArrayUtils.printArray(ArrayUtils.insertByIndex(array, 10, -1, -1, -1));
//        ArrayUtils.printArray(ArrayUtils.deleteElement(array, 1));
        ArrayUtils.printArray(ArrayUtils.reverse(array));
    }
}
